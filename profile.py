# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the InstaGENI library.
import geni.rspec.igext as ig
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

agglist = [
    ("urn:publicid:IDN+emulab.net+authority+cm","emulab.net"),
    ("urn:publicid:IDN+utah.cloudlab.us+authority+cm","utah.cloudlab.us"),
    ("urn:publicid:IDN+clemson.cloudlab.us+authority+cm","clemson.cloudlab.us"),
    ("urn:publicid:IDN+wisc.cloudlab.us+authority+cm","wisc.cloudlab.us"),
    ("urn:publicid:IDN+apt.emulab.net+authority+cm","apt.emulab.net"),
    ("urn:publicid:IDN+test1.powderwireless.net+authority+cm","test1.powderwireless.net"),
    ("urn:publicid:IDN+web.powderwireless.net+authority+cm","web.powderwireless.net"),
    ("urn:publicid:IDN+ebc.powderwireless.net+authority+cm","ebc.powderwireless.net"),
    ("urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm","guesthouse.powderwireless.net"),
    ("urn:publicid:IDN+bookstore.powderwireless.net+authority+cm","bookstore.powderwireless.net"),
    ("urn:publicid:IDN+humanities.powderwireless.net+authority+cm","humanities.powderwireless.net"),
    ("urn:publicid:IDN+law73.powderwireless.net+authority+cm","law73.powderwireless.net"),
    ("urn:publicid:IDN+madsen.powderwireless.net+authority+cm","madsen.powderwireless.net"),
    ("urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm","sagepoint.powderwireless.net"),
    ("urn:publicid:IDN+moran.powderwireless.net+authority+cm","moran.powderwireless.net"),
    ("urn:publicid:IDN+cpg.powderwireless.net+authority+cm","cpg.powderwireless.net"),
    ("urn:publicid:IDN+telemetry.powderwireless.net+authority+cm","telemetry.powderwireless.net"),
    ("urn:publicid:IDN+bus-test.powderwireless.net+authority+cm","bus-test.powderwireless.net"),
    ("urn:publicid:IDN+bus-test2.powderwireless.net+authority+cm","bus-test2.powderwireless.net"),
    ("urn:publicid:IDN+bus-4964.powderwireless.net+authority+cm","bus-4964.powderwireless.net"),
    ("urn:publicid:IDN+bus-4734.powderwireless.net+authority+cm","bus-4734.powderwireless.net"),
    ("urn:publicid:IDN+apt.emulab.net+authority+cm","apt.emulab.net"),
    ("urn:publicid:IDN+cloudlab.umass.edu+authority+cm","cloudlab.umass.edu")
]
aggmap = { a[1]:a[0] for a in agglist }

pc.defineParameter("nodes","Number of Nodes",portal.ParameterType.INTEGER,1,
                   longDescription="Number of nodes.")
pagglist = agglist
pagglist.insert(0,("","Any"))
pc.defineParameter("aggregate","Specific Aggregate",portal.ParameterType.STRING,
                   "",pagglist)
pc.defineParameter("fixedNodes","Specific Fixed Nodes",portal.ParameterType.STRING,"",
                   longDescription="A space-separated list of CloudLab node_ids or component_ids URN.  If you provide a simple node_id, we will attempt to map it to the correct component_id URN.")
pc.defineParameter("nodeTypes","Node Type",portal.ParameterType.NODETYPE,"",
                   longDescription="Node type of physical nodes (overridden if Node Type List is specified).")
pc.defineParameter("nodeTypeList","Node Type List",portal.ParameterType.STRING,"",
                   longDescription="Node type(s) of physical nodes, separated by whitespace if more than one.  If more than one, we will alternate through the list as we create nodes.")
pc.defineParameter("nodeNames","Node Name(s)",portal.ParameterType.STRING, "",
                   longDescription="A space-separated list of logical node names, if you do not care for node-`i`.  If more than one, we will iterate through the list as we create nodes, and fall back to node-`i` at the end of this list.")
pc.defineParameter("images","Node Image(s)",portal.ParameterType.STRING,
                   'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD',
                   longDescription="The image (or images) your nodes will run, separated by whitespace if more than one.  If more than one, we will alternate through the list as we create nodes.")
pc.defineParameter("attributes","Node Attributes(s)",portal.ParameterType.STRING,'',
                   longDescription="A space-separated list of attrkey=attrvalue tuples to be requested for each node.")
pc.defineParameter("desires","Node Desire(s)",portal.ParameterType.STRING,'',
                   longDescription="A space-separated list of desire:weight tuples to be requested for each node.")
pc.defineParameter("startupCmd","Node Startup Command",portal.ParameterType.STRING,"",
                   longDescription="A command each node will run on startup.")
pc.defineParameter("installVNC","Install VNC",
                   portal.ParameterType.BOOLEAN,False,
                   longDescription="Install VNC on each node.  This is useful if you are participating in a tutorial, demo, or simply do not want to open SSH connections in ten separate terminals on your desktop or in the web UI.")
pc.defineParameter("numberLans","Number of LANs",portal.ParameterType.INTEGER,0,
                   longDescription="Number of LANs to which each node is connected.")
pc.defineParameter("buildSnakeLink","Build Snake of Links",portal.ParameterType.BOOLEAN,False,
                   longDescription="Build a snake of links from node to node.")
pc.defineParameter("snakeEatTail","Connect the Snake head and tail nodes",portal.ParameterType.BOOLEAN,False,
                   longDescription="Connect the Snake's head and tail nodes.")
pc.defineParameter("buildChainLink","Build Chain of Links",portal.ParameterType.BOOLEAN,False,
                   longDescription="Build a chain of links from node to node.")
pc.defineParameter("linkSpeed", "LAN/Link Speed",portal.ParameterType.INTEGER, 0,
                   [(0,"Any"),(100000,"100Mb/s"),(1000000,"1Gb/s"),(10000000,"10Gb/s"),(25000000,"25Gb/s"),(100000000,"100Gb/s")],
                   longDescription="A specific link speed to use for each LAN and link.")
pc.defineParameter("linkDelay", "LAN/Link Delay",portal.ParameterType.INTEGER, 0,
                   longDescription="A specific link delay (ms) to use for each LAN and link.")
pc.defineParameter("multiplex", "Multiplex Networks",portal.ParameterType.BOOLEAN, False,
                   longDescription="Multiplex all LANs and links.")
pc.defineParameter("bestEffort", "Best Effort Link Bandwidth",portal.ParameterType.BOOLEAN, False,
                   longDescription="Do not require guaranteed bandwidth throughout switch fabric.")
pc.defineParameter("vlanTagging", "VLAN Tagging",portal.ParameterType.BOOLEAN, False,
                   longDescription="Expose VLAN tags to experiment nodes (by default experiment link ports untag host-bound traffic.")
pc.defineParameter("skipVlans", "Do not call snmpit to setup vlans",portal.ParameterType.BOOLEAN, False,
                   longDescription="Do not call snmpit to setup vlans (debugging option).")
pc.defineParameter("forceShaping", "Force Traffic Shaping",portal.ParameterType.BOOLEAN, False,
                   longDescription="Force traffic shaping, even if none is deemed necessary.")
pc.defineParameter("setJumboFrames", "Enable Jumbo Frames",portal.ParameterType.BOOLEAN, False,
                   longDescription="Enable jumbo frames on links.")
pc.defineParameter("nodesSite2","Number of Nodes at Site 2",portal.ParameterType.INTEGER,0,
                   longDescription="Number of nodes at site 2.")
pc.defineParameter("aggregateSite2","Specific Aggregate for Site 2",portal.ParameterType.STRING,
                   "",pagglist)
pc.defineParameter(
    "publicIPCount","Number of public IP addresses",
    portal.ParameterType.INTEGER,0)
pc.defineStructParameter(
    "sharedVlans","Add Shared VLAN",[],
    multiValue=True,itemDefaultValue={},min=0,max=None,
    members=[
        portal.Parameter(
            "createConnectableSharedVlan","Create Connectable Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Create a placeholder, connectable shared VLAN stub and 'attach' the first node to it.  You can use this during the experiment to connect this experiment interface to another experiment's shared VLAN."),
        portal.Parameter(
            "createSharedVlan","Create Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Create a new shared VLAN with the name above, and connect the first node to it."),
        portal.Parameter(
            "connectSharedVlan","Connect to Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Connect an existing shared VLAN with the name below to the first node."),
        portal.Parameter(
            "sharedVlanName","Shared VLAN Name",
            portal.ParameterType.STRING,"",
            longDescription="A shared VLAN name (functions as a private key allowing other experiments to connect to this node/VLAN), used when the 'Create Shared VLAN' or 'Connect to Shared VLAN' options above are selected.  Must be fewer than 32 alphanumeric characters."),
        portal.Parameter(
            "sharedVlanAddress","Shared VLAN IP Address",
            portal.ParameterType.STRING,"10.254.254.1",
            longDescription="Set the IP address for the shared VLAN interface.  Make sure to use an unused address within the subnet of an existing shared vlan!"),
        portal.Parameter(
            "sharedVlanNetmask","Shared VLAN Netmask",
            portal.ParameterType.STRING,"255.255.255.0",
            longDescription="Set the subnet mask for the shared VLAN interface, as a dotted quad.")])
pc.defineStructParameter(
    "datasets","Datasets",[],
    multiValue=True,itemDefaultValue={},min=0,max=None,
    members=[
        portal.Parameter(
            "urn","Dataset URN",
            portal.ParameterType.STRING,"",
            longDescription="The URN of an *existing* remote dataset (a remote block store) that you want attached to the node you specified (defaults to the first node).  The block store must exist at the cluster at which you instantiate the profile."),
        portal.Parameter(
            "mountNode","Dataset Mount Node",
            portal.ParameterType.STRING,"node-0",
            longDescription="The node on which you want your remote block store mounted; defaults to the first node."),
        portal.Parameter(
            "mountPoint","Dataset Mount Point",
            portal.ParameterType.STRING,"/dataset",
            longDescription="The mount point at which you want your remote dataset mounted.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).  Note also that this option requires a network interface, because it creates a link between the dataset and the node where the dataset is available.  Thus, just as for creating extra LANs, you might need to select the Multiplex Flat Networks option, which will also multiplex the blockstore link here."),
        portal.Parameter(
            "readOnly","Mount Dataset Read-only",
            portal.ParameterType.BOOLEAN,True,
            longDescription="Mount the remote dataset in read-only mode."),
        portal.Parameter(
            "rwClone","Mount Writeable Ephemeral Clone of Dataset",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Mount a writeable, ephemeral clone of the remote dataset.")])
pc.defineStructParameter(
    "tempBlockstores","Temporary Datasets",[],
    multiValue=True,itemDefaultValue={},min=0,max=None,
    members=[
        portal.Parameter(
            "mountPoint", "Mount Point",
            portal.ParameterType.STRING,"",
            longDescription="Mounts an ephemeral, temporary filesystem at this mount point, on the nodes which you specify below.  If you specify no nodes, and specify a mount point here, all nodes will get a temp filesystem.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken)."),
        portal.Parameter(
            "size", "Dataset Size",
            portal.ParameterType.INTEGER, 0,
            longDescription="The necessary space in GB to reserve for your temporary filesystem."),
        portal.Parameter(
            "mountNodes", "Mount Node(s)",
            portal.ParameterType.STRING,"",
            longDescription="The node(s) on which you want a temporary filesystem created; space-separated for more than one.  Leave blank if you want all nodes to have this temp filesystem."),
        portal.Parameter(
            "placement", "Placement",
            portal.ParameterType.STRING,"any",
            [("any","Any"),("sysvol","System Volume"),("nonsysvol","Non-system Volume")],
            longDescription="Mounts an ephemeral, temporary filesystem at this mount point, on the nodes which you specify below.  If you specify no nodes, and specify a mount point here, all nodes will get a temp filesystem.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken)."),
        portal.Parameter(
            "remote", "Remote",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Creates an  iSCSI-based temporary dataset on the storage server.  Placement is ignored if this option is set.")])
pc.defineStructParameter(
    "cephNodes","Ceph Nodes",[],
    multiValue=True,itemDefaultValue={},min=0,max=None,
    members=[
        portal.Parameter(
            "mountNode", "Mount Node",
            portal.ParameterType.STRING,"",
            longDescription="The node on which you want the CephFS mounted.")])

params = pc.bindParameters()

if params.nodes < 1:
    pc.reportError(portal.ParameterError("Must specify at least one physical host",['nodes']))
if params.nodesSite2 > 0 \
  and (params.numberLans > 0 or params.buildSnakeLink or params.buildChainLink) \
  and not params.multiplex:
    pc.reportError(portal.ParameterError(
        "Must specify multiplex option when including nodes at multiple sites.",
        ['nodesSite2','multiplex']))
i = 0
for x in params.sharedVlans:
    n = 0
    if x.createConnectableSharedVlan:
        n += 1
    if x.createSharedVlan:
        n += 1
    if x.connectSharedVlan:
        n += 1
    if n > 1:
        err = portal.ParameterError(
            "Must choose only a single shared vlan operation (create, connect, create connectable)",
        [ 'sharedVlans[%d].createConnectableSharedVlan' % (i,),
          'sharedVlans[%d].createSharedVlan' % (i,),
          'sharedVlans[%d].connectSharedVlan' % (i,) ])
        pc.reportError(err)
    if n == 0:
        err = portal.ParameterError(
            "Must choose one of the shared vlan operations: create, connect, create connectable",
        [ 'sharedVlans[%d].createConnectableSharedVlan' % (i,),
          'sharedVlans[%d].createSharedVlan' % (i,),
          'sharedVlans[%d].connectSharedVlan' % (i,) ])
        pc.reportError(err)
    i += 1

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

tour = ig.Tour()
tour.Description(ig.Tour.TEXT,"Instantiate one or more nodes with lots of potential parameters.")
request.addTour(tour)

def setLanLinkParams(lanlink):
    if params.bestEffort:
        lanlink.best_effort = True
    if params.multiplex:
        lanlink.link_multiplexing = True
    if params.vlanTagging:
        lanlink.vlan_tagging = True
    if params.linkSpeed > 0:
        lanlink.bandwidth = params.linkSpeed
    if params.linkDelay > 0:
        lanlink.latency = params.linkDelay
    if params.forceShaping:
        lanlink.setForceShaping()
    if params.setJumboFrames:
        lanlink.setJumboFrames()

lans = list()
for i in range(0,params.numberLans):
    lan = pg.LAN('lan-%d' % (i,))
    setLanLinkParams(lan)
    lans.append(lan)
    pass

def getCMId(urn):
    for (k,v) in aggmap.iteritems():
        if "+%s+" % (k,) in urn:
            return v
    return None

def getNodeCMTuple(node):
    cmap = {
        "urn:publicid:IDN+utah.cloudlab.us+node+":["ms","hp"],
        "urn:publicid:IDN+wisc.cloudlab.us+node+":["c220g1-","c220g2-","c220g5-","c240g1-","c240g2-","c240g5-","c4130-"],
        "urn:publicid:IDN+clemson.cloudlab.us+node+":["clgpu","clnode","clstore"],
        "urn:publicid:IDN+apt.emulab.net+node+":["apt"],
        "urn:publicid:IDN+emulab.net+node+":["pc","cbrssdr"]
    }
    if node.startswith("urn:"):
        return (getCMId(node),node)
    for (k,v) in cmap.iteritems():
        for prefix in v:
            if node.startswith(prefix):
                ncid = k + node
                return (getCMId(ncid),ncid)
    return None

if params.installVNC:
    request.initVNC()

nodes = dict()
nodeNamesInOrder = list()
links = list()

firstSnakeLink = None
lastSnakeLink = None
lastSnakeLinkNum = 0
lastChainLink = None
lastChainLinkNum = 0
imageList = []
nodeNameList = []
fixedNodeList = []
fixedNodeCMTupleList = []
nodeTypeList = []
desires = []
attributes = []
if params.fixedNodes:
    fixedNodeList = params.fixedNodes.split()
    for n in fixedNodeList:
        ret = getNodeCMTuple(n)
        if ret is None or ret[0] == None:
            #pc.reportError(portal.ParameterError("Unrecognized node_id '%s'" % (n),['fixedNodes']))
            fixedNodeCMTupleList.append((None,n))
        else:
            fixedNodeCMTupleList.append(ret)
cephNodes = dict()
if params.cephNodes:
    for cn in params.cephNodes:
        cephNodes[cn.mountNode] = cn.mountNode
if params.nodeTypeList:
    nodeTypeList = params.nodeTypeList.split()
elif params.nodeTypes:
    nodeTypeList = params.nodeTypes.split()
if params.desires:
    dl = params.desires.split()
    for d in dl:
        desires.append(d.split(":"))
if params.attributes:
    al = params.attributes.split()
    for a in al:
        attributes.append(a.split("="))
if params.images:
    imageList = params.images.split(" ")
    tmplist = []
    for i in imageList:
        if i.startswith("urn"):
            tmplist.append(i)
        elif params.nodeTypes == "aarch64":
            tmplist.append("urn:publicid:IDN+utah.cloudlab.us+image+emulab-ops//" + i)
        elif params.nodeTypes == "ibm8335":
            tmplist.append("urn:publicid:IDN+clemson.cloudlab.us+image+emulab-ops//" + i)
        elif i != "None":
            tmplist.append("urn:publicid:IDN+emulab.net+image+emulab-ops//" + i)
        else:
            tmplist.append(i)
    imageList = tmplist
if params.nodeNames:
    nodeNameList = params.nodeNames.split(" ")
isMultiSite = False
if params.nodesSite2 > 0:
    isMultiSite = True
sharedvlans = []
bsnodes = []
bslinks = []
tbs_idx = 0
for (site,pnodes,aggregate) in [(1,params.nodes,params.aggregate),
                          (2,params.nodesSite2,params.aggregateSite2)]:
    siteStr = ""
    if site > 1:
        siteStr = "s%d-" % (site,)
    for i in range(0,pnodes):
        node_name = 'node-%s%d' % (siteStr,i)
        if len(nodeNameList) > i:
            node_name = nodeNameList[i]
        node = pg.RawPC(node_name)
        if isMultiSite:
            node.Site("%d" % (site,))
        if i < len(fixedNodeCMTupleList):
            (cmid,cmpid) = fixedNodeCMTupleList[i]
            node.component_manager_id = cmid
            node.component_id = cmpid
        elif len(nodeTypeList):
            node.hardware_type = nodeTypeList[i % len(nodeTypeList)]
        if aggregate:
            node.component_manager_id = aggregate
        if len(imageList) > 0:
            if imageList[i % len(imageList)] != "None":
                node.disk_image = imageList[i % len(imageList)]
            pass
        if node_name in cephNodes:
            node.mountCephFS()
        for d in desires:
            node.Desire(d[0],d[1])
        for a in attributes:
            node.Attribute(a[0],a[1])
        if params.startupCmd:
            node.addService(pg.Execute(shell="sh",command=params.startupCmd))
        # geni-lib assumes the "newer" request format, instead
        # of the old addResource method.
        if params.installVNC:
            try:
                node.startVNC()
            except:
                pass
            try:
                node._ext_children.append(emulab.emuext.startVNC())
            except:
                pass

        nodes[node_name] = node
        nodeNamesInOrder.append(node_name)

        for k in range(0,params.numberLans):
            iface = node.addInterface('if%d' % (k,))
            lans[k].addInterface(iface)
            pass
        if params.buildSnakeLink:
            if not lastSnakeLink:
                lastSnakeLink = pg.Link("slink-%d" % (lastSnakeLinkNum,))
                setLanLinkParams(lastSnakeLink)
                siface = node.addInterface('ifS%d' % (lastSnakeLinkNum,))
                lastSnakeLink.addInterface(siface)
            else:
                siface = node.addInterface('ifS%d' % (lastSnakeLinkNum,))
                lastSnakeLink.addInterface(siface)
                links.append(lastSnakeLink)
            
                lastSnakeLinkNum += 1
                lastSnakeLink = pg.Link("slink-%d" % (lastSnakeLinkNum,))
                setLanLinkParams(lastSnakeLink)
                siface = node.addInterface('ifS%d' % (lastSnakeLinkNum,))
                lastSnakeLink.addInterface(siface)
            pass
        if params.buildChainLink:
            if not lastChainLink:
                lastChainLink = pg.Link("chlink-%d" % (lastChainLinkNum,))
                setLanLinkParams(lastChainLink)
                siface = node.addInterface('ifCh')
                lastChainLink.addInterface(siface)
            else:
                siface = node.addInterface('ifCh')
                lastChainLink.addInterface(siface)
                setLanLinkParams(lastChainLink)
                links.append(lastChainLink)
                lastChainLink = None
                lastChainLinkNum += 1
            pass
        if site == 1 and i == 0:
            k = 0
            for x in params.sharedVlans:
                iface = node.addInterface("ifSharedVlan%d" % (k,))
                if x.sharedVlanAddress:
                    iface.addAddress(
                        pg.IPv4Address(x.sharedVlanAddress,x.sharedVlanNetmask))
                sharedvlan = pg.Link('shared-vlan-%d' % (k,))
                sharedvlan.addInterface(iface)
                if x.createConnectableSharedVlan:
                    sharedvlan.enableSharedVlan()
                else:
                    if x.createSharedVlan:
                        sharedvlan.createSharedVlan(x.sharedVlanName)
                    else:
                        sharedvlan.connectSharedVlan(x.sharedVlanName)
                if params.multiplex:
                    sharedvlan.link_multiplexing = True
                    sharedvlan.best_effort = True
                sharedvlans.append(sharedvlan)
                k += 1
        for tbs in params.tempBlockstores:
            if tbs.mountNodes and tbs.mountNodes != node_name:
                continue
            bsname = "%s-temp-bs-%d" % (node_name,tbs_idx)
            if tbs.remote:
                bs = ig.RemoteBlockstore(bsname,tbs.mountPoint)
                myintf = node.addInterface("ifbstmp%d" % (tbs_idx,))
                bsintf = bs.interface
                bsnodes.append(bs)
                bslink = pg.Link("bstmplink-%d" % (tbs_idx,))
                bslink.addInterface(myintf)
                bslink.addInterface(bsintf)
                bslink.best_effort = True
                bslink.vlan_tagging = True
                bslinks.append(bslink)
            else:
                bs = node.Blockstore(bsname,tbs.mountPoint)
                bs.placement = tbs.placement
            bs.size = str(tbs.size) + "GB"
            tbs_idx += 1
        pass

if params.buildSnakeLink and params.snakeEatTail:
    firstNode = nodes[nodeNamesInOrder[0]]
    lastNode = nodes[nodeNamesInOrder[-1]]
    snakeEatingLink = pg.Link("eating-link")
    setLanLinkParams(snakeEatingLink)
    siface = firstNode.addInterface('ifEat1')
    snakeEatingLink.addInterface(siface)
    siface = lastNode.addInterface('ifEat0')
    snakeEatingLink.addInterface(siface)
    links.append(snakeEatingLink)

#
# Add the dataset(s), if requested.
#
i = 0
for x in params.datasets:
    if not x.urn:
        err = portal.ParameterError(
            "Must provide a non-null dataset URN",
            [ 'datasets[%d].urn' % (i,) ])
        pc.reportError(err)
        pc.verifyParameters()
    if x.mountNode not in nodes:
        perr = portal.ParameterError(
            "The node on which you mount your dataset must exist, and does not.",
            [ 'datasets[%d].mountNode' % (i,) ])
        pc.reportError(perr)
        pc.verifyParameters()
    bsn = nodes[x.mountNode]
    myintf = bsn.addInterface("ifbs%d" % (i,))
    bsnode = ig.RemoteBlockstore("bsnode-%d" % (i,),x.mountPoint)
    bsintf = bsnode.interface
    bsnode.dataset = x.urn
    bsnode.readonly = x.readOnly
    bsnode.rwclone = x.rwClone
    bsnodes.append(bsnode)

    bslink = pg.Link("bslink-%d" % (i,))
    bslink.addInterface(myintf)
    bslink.addInterface(bsintf)
    bslink.best_effort = True
    bslink.vlan_tagging = True
    bslinks.append(bslink)
    i += 1

for x in nodes.values():
    request.addResource(x)
for x in lans:
    request.addResource(x)
for x in links:
    request.addResource(x)
for x in sharedvlans:
    request.addResource(x)
for x in bsnodes:
    request.addResource(x)
for x in bslinks:
    request.addResource(x)
if params.publicIPCount:
    request.addResource(ig.AddressPool("node-0",params.publicIPCount))
if params.skipVlans:
    request.skipVlans()

pc.printRequestRSpec(request)
